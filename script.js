(function ($j, $ds) {
	/*
		$j == jQuery
		$ds == data store (usually local storage)
	*/
	
	var self = {
		data: null,
		
		onReady: function () {
			self.initTaskCreation();
			self.initTaskDeletion();
			self.loadData();
			self.renderData();
		},
		
		initTaskCreation: function () {
			var entry = $j('#new');
			
			$j(document).keypress(function () {
				entry.focus();
			});
			
			entry.keypress(function (e) {
				if (e.which != 13) return;
				
				self.addItem(entry.val());
				entry.val('');
			});
		},
		
		initTaskDeletion: function () {
			$('.list ul').on('click', 'li', function (e) {
				var el = $j(this);
				
				el.slideUp('slow', function () {
					self.removeItem(el.attr('data-id'));
				});
			});
		},
		
		loadData: function () {
			self.data = JSON.parse($ds.getItem('tasks')) || [];
		},
		
		saveData: function () {
			$ds.setItem('tasks', JSON.stringify(self.data));
		},
		
		renderData: function () {
			var list = $j('.list ul');
			list.html('');
			
			for (var i = 0; i < self.data.length; i++)
				list.append('<li data-id="' + i + '">' + self.data[i] + '</li>');
		},
		
		addItem: function (item) {
			self.data.push(item);
			self.saveData();
			self.renderData();
		},
		
		removeItem: function (index) {
			self.data.splice(index, 1);
			self.saveData();
			self.renderData();
		}
	};
	
	$j(self.onReady);
})(jQuery, localStorage);
